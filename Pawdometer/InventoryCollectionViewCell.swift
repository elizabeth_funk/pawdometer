//
//  InventoryCollectionViewCell.swift
//  Pawdometer
//
//  Created by Tracy on 4/7/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit

class InventoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemCount: UILabel!
}
