//
//  PetStoreCollectionViewCell.swift
//  Pawdometer
//
//  Created by Kimberly Hwang on 4/6/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit

class PetStoreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemPrice: UILabel!
    
}
