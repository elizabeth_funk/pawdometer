//
//  InsightsViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Charts
import HealthKit


let healthStore = HKHealthStore()
let queue = DispatchQueue.global()

var stepsDone = false
var flightsDone = false
var dates:[String] = []
var flights:[Double] = []
var steps:[Double] = []

var stepsMonthDone = false
var flightsMonthDone = false
var datesMonth:[String] = []
var flightsMonth:[Double] = []
var stepsMonth:[Double] = []

var stepsYearDone = false
var flightsYearDone = false
var datesYear:[String] = []
var flightsYear:[Double] = []
var stepsYear:[Double] = []

class InsightsViewController: UIViewController, IAxisValueFormatter {

    @IBOutlet weak var segControl: UISegmentedControl!
    
    @IBOutlet weak var insightsTitle: UILabel!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var lineChartView: LineChartView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        healthData()
        self.insightsTitle.alpha = 0.0
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
            options: [],
            animations: {
                self.insightsTitle.alpha = 1.0
            })
    }
    
    private func healthData() {
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!, HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.flightsClimbed)!, HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)! ]
         
        // Check for Authorization
        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (success, error) in
            if (success) {
                // Authorization Successful
                //set all done checks to false
                stepsDone = false
                flightsDone = false
                stepsMonthDone = false
                flightsMonthDone = false
                stepsYearDone = false
                flightsYearDone = false
                
                self.getStepsWeek() {
                    DispatchQueue.main.sync {
                     self.customizeLineChart(dataPoints: dates, values: steps.map{Double($0)})
                    }
                }
                self.getFlightsWeek() {
                    while(!stepsDone) {
                    }
                    DispatchQueue.main.sync {
                        self.customizeBarChart(dataPoints: dates, values: flights.map{Double($0)})
                    }
                }
                self.getStepsMonth() {}
                self.getFlightsMonth() {}
                self.getStepsYear() {}
                self.getFlightsYear() {}
            } // end if
         
        } // end of checking authorization
    }
    
    func getStepsWeek(completion: @escaping ()-> Void){
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let format = DateFormatter()
        format.dateFormat = "MM-dd"
        var currentDate = now
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfDay,
        intervalComponents: interval)
        
        //clear current values
        steps = []
        dates = []
        
        query.initialResultsHandler = { _, result, error in
                let startDate = Calendar.current.date(byAdding: .day, value: -6, to: now)!
                result!.enumerateStatistics(from: startDate, to: now) { statistics, stop in
                
                if let quantity = statistics.sumQuantity() {
                    let value = quantity.doubleValue(for: HKUnit.count())
                    steps.append(value)
                }
                else {
                    steps.append(0)
                }
                currentDate = Calendar.current.date(byAdding: .day, value: 1, to: currentDate)!
                dates.append(format.string(from: currentDate))
            }
            queue.async {
                stepsDone = true
                completion()
            }
        }
        healthStore.execute(query)
    }
    
    func getFlightsWeek(completion: @escaping ()-> Void){
        let flightQuantityType = HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(
        quantityType: flightQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfDay,
        intervalComponents: interval)
        
        //clear current values
        flights = []
        
        query.initialResultsHandler = { _, result, error in
                let startDate = Calendar.current.date(byAdding: .day, value: -6, to: now)!
                result!.enumerateStatistics(from: startDate, to: now) { statistics, stop in
                if let quantity = statistics.sumQuantity() {
                    // Get flights (they are of double type)
                    let value = quantity.doubleValue(for: HKUnit.count())
                    flights.append(value)
                } // end if
                else{
                    flights.append(0)
                }
            }
            queue.async {
                flightsDone = true
                completion()
            }
        }
        healthStore.execute(query)
    }
    
    func getStepsMonth(completion: @escaping ()-> Void){
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let format = DateFormatter()
        format.dateFormat = "MM-dd"
        var currentDate = now
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        var monthComponents = DateComponents()
        monthComponents.month = Calendar.current.component(.month, from: startOfDay)
        monthComponents.year = Calendar.current.component(.year, from: startOfDay)
        let startOfMonth = Calendar.current.date(from: monthComponents)
        let oneMonthPrev = Calendar.current.date(byAdding: .month, value: -1, to: now)!
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfMonth!,
        intervalComponents: interval)
        
        //clear current values
        stepsMonth = []
        datesMonth = []
        
        query.initialResultsHandler = { _, result, error in
            result!.enumerateStatistics(from: oneMonthPrev, to: now) { statistics, stop in
                
                if let quantity = statistics.sumQuantity() {
                    let value = quantity.doubleValue(for: HKUnit.count())
                    stepsMonth.append(value)
                }
                else {
                    stepsMonth.append(0)
                }
                currentDate = Calendar.current.date(byAdding: .day, value: 1, to: currentDate)!
                datesMonth.append(format.string(from: currentDate))
            }
            queue.async {
                stepsMonthDone = true
                completion()
            }
        }
        healthStore.execute(query)
    }
    
    func getFlightsMonth(completion: @escaping ()-> Void){
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        var monthComponents = DateComponents()
        monthComponents.month = Calendar.current.component(.month, from: startOfDay)
        monthComponents.year = Calendar.current.component(.year, from: startOfDay)
        let startOfMonth = Calendar.current.date(from: monthComponents)
        let oneMonthPrev = Calendar.current.date(byAdding: .month, value: -1, to: now)!
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfMonth!,
        intervalComponents: interval)
        
        //clear current values
        flightsMonth = []
        
        query.initialResultsHandler = { _, result, error in
            result!.enumerateStatistics(from: oneMonthPrev, to: now) { statistics, stop in
                
                if let quantity = statistics.sumQuantity() {
                    let value = quantity.doubleValue(for: HKUnit.count())
                    flightsMonth.append(value)
                }
                else {
                    flightsMonth.append(0)
                }
            }
            queue.async {
                flightsMonthDone = true
                completion()
            }
        }
        healthStore.execute(query)
    }
    
    func getStepsYear(completion: @escaping ()-> Void){
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let format = DateFormatter()
        format.dateFormat = "MM"
        var currentDate = now
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.month = 1
        
        var monthComponents = DateComponents()
        monthComponents.month = Calendar.current.component(.month, from: startOfDay)
        monthComponents.year = Calendar.current.component(.year, from: startOfDay)
        let startOfMonth = Calendar.current.date(from: monthComponents)
        let oneYearPrev = Calendar.current.date(byAdding: .year, value: -1, to: now)!
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfMonth!,
        intervalComponents: interval)
        
        //clear current values
        stepsYear = []
        datesYear = []
        
        query.initialResultsHandler = { _, result, error in
            result!.enumerateStatistics(from: oneYearPrev, to: now) { statistics, stop in
                
                if let quantity = statistics.sumQuantity() {
                    //let date = statistics.startDate
                    let value = quantity.doubleValue(for: HKUnit.count())
                    stepsYear.append(value)
                }
                else {
                    stepsYear.append(0)
                }
                currentDate = Calendar.current.date(byAdding: .month, value: 1, to: currentDate)!
                datesYear.append(format.string(from: currentDate))
            }
            queue.async {
                stepsYearDone = true
                completion()
            }
        }
        healthStore.execute(query)
    }
    
    func getFlightsYear (completion: @escaping ()-> Void){
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.month = 1
        
        var monthComponents = DateComponents()
        monthComponents.month = Calendar.current.component(.month, from: startOfDay)
        monthComponents.year = Calendar.current.component(.year, from: startOfDay)
        let startOfMonth = Calendar.current.date(from: monthComponents)
        let oneYearPrev = Calendar.current.date(byAdding: .year, value: -1, to: now)!
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfMonth!,
        intervalComponents: interval)
        
        //clear current values
        flightsYear = []
        
        query.initialResultsHandler = { _, result, error in
            result!.enumerateStatistics(from: oneYearPrev, to: now) { statistics, stop in
                
                if let quantity = statistics.sumQuantity() {
                    let value = quantity.doubleValue(for: HKUnit.count())
                    flightsYear.append(value)
                }
                else {
                    flightsYear.append(0)
                }
            }
            queue.async {
                flightsYearDone = true
                completion()
            }
        }
        healthStore.execute(query)
    }
    
    func customizeBarChart(dataPoints: [String], values: [Double]){
      
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
          let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
          dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Bar Chart View")
        let chartData = BarChartData(dataSet: chartDataSet)
        
        barChartView.xAxis.valueFormatter = self
        barChartView.data = chartData
        barChartView.xAxis.labelPosition = .bottom
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.enabled = false
        barChartView.legend.enabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.axisMinimum = 0.0
        //barChartView.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .easeInOutBounce)

        chartDataSet.colors = [UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)]

    }
    
    func customizeLineChart(dataPoints: [String], values: [Double]){
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
          let dataEntry = ChartDataEntry(x: Double(i), y: Double(values[i]))
          dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: nil)
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        
        lineChartView.xAxis.valueFormatter = self
        lineChartView.data = lineChartData
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.legend.enabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        //lineChartView.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .easeInOutBounce)
        
        lineChartDataSet.colors = [UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)]
        lineChartDataSet.setCircleColor(UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1))
        lineChartDataSet.circleHoleColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        lineChartDataSet.circleRadius = 3.5
        

    }
    
//    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
//      var colors: [UIColor] = []
//      for _ in 0..<numbersOfColor {
//        let red = Double(arc4random_uniform(256))
//        let green = Double(arc4random_uniform(256))
//        let blue = Double(arc4random_uniform(256))
//        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
//        colors.append(color)
//      }
//      return colors
//    }

    @IBAction func onSegmentChanged(_ sender: Any) {
        
        switch segControl.selectedSegmentIndex {
        case 0:
            if(!(flightsDone && stepsDone)) {
                queue.async {
                    while(!(flightsDone && stepsDone)) {
            
                    }
                    DispatchQueue.main.sync {
                        self.customizeBarChart(dataPoints: dates, values: flights.map{Double($0)})
                        self.customizeLineChart(dataPoints: dates, values: steps.map{Double($0)})
                    }
                }
            } else {
                customizeBarChart(dataPoints: dates, values: flights.map{Double($0)})
                customizeLineChart(dataPoints: dates, values: steps.map{Double($0)})
            }
        case 1:
            if(!(flightsMonthDone && stepsMonthDone)) {
                queue.async {
                    while(!(flightsMonthDone && stepsMonthDone)) {
                    }
                    DispatchQueue.main.sync {
                        self.customizeBarChart(dataPoints: datesMonth, values: flightsMonth.map{Double($0)})
                        self.customizeLineChart(dataPoints: datesMonth, values: stepsMonth.map{Double($0)})
                    }
                }
            } else {
                customizeBarChart(dataPoints: datesMonth, values: flightsMonth.map{Double($0)})
                customizeLineChart(dataPoints: datesMonth, values: stepsMonth.map{Double($0)})
            }
        case 2:
            if(!(flightsYearDone && stepsYearDone)) {
                queue.async {
                    while(!(flightsYearDone && stepsYearDone)) {
                        
                    }
                    DispatchQueue.main.sync {
                        self.customizeBarChart(dataPoints: datesYear, values: flightsYear.map{Double($0)})
                        self.customizeLineChart(dataPoints: datesYear, values: stepsYear.map{Double($0)})
                    }
                }
            } else {
                customizeBarChart(dataPoints: datesYear, values: flightsYear.map{Double($0)})
                customizeLineChart(dataPoints: datesYear, values: stepsYear.map{Double($0)})
            }
        default:
            print("This shouldn't happen.")
        }
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        switch segControl.selectedSegmentIndex {
        case 0:
            while index > dates.count{
            }
            return dates[index]
        case 1:
            while index > datesMonth.count {
            }
            return datesMonth[index]
        case 2:
            while index > datesYear.count {
            }
            return datesYear[index]
        default:
            return ""
        }
    }
}
