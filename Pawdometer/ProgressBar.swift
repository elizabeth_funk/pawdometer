//
//  ProgressBar.swift
//  Pawdometer
//
//  Created by Tracy Zhang on 3/31/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit

extension UIColor {
    var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return (red, green, blue, alpha)
    }
}

class ProgressBar: UIView {

    private let progressView: UIView
    private var progressBarWidth: NSLayoutConstraint? = nil

    var percentage: Double = 0 {
        didSet {
            updateProgress()
            
            //Send 25% and 50% happiness notifications
            if percentage < 50 && oldValue >= 50 {
                schedule50Notif()
            } else if percentage < 25 && oldValue >= 25 {
                schedule25Notif()
            }
        }
    }

    init() {
        progressView = UIView()
        progressView.layer.cornerRadius = 10 // Curve the bar corners
        progressView.layer.borderWidth = 2
        super.init(frame: .zero)
        setupBackgroundBar()
        setupProgressView()
        updateProgress()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupBackgroundBar() {
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 3
        self.heightAnchor.constraint(equalToConstant: 25).isActive = true // Width of the bar
        self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
    }

    private func setupProgressView(){
        self.addSubview(progressView)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        progressView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        progressView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        progressView.backgroundColor = UIColor.systemRed
    }

    func updateProgress() {
        let progressMultiplier = CGFloat(percentage/100)
        
        UIView.animate(withDuration: 1.0) {
            if let progressConstraint = self.progressBarWidth {
                NSLayoutConstraint.deactivate([progressConstraint])
            }

            self.progressBarWidth = self.progressView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: progressMultiplier)
            self.progressBarWidth?.isActive = true
            let oldColor = self.progressView.backgroundColor!.rgba
            let progress = CGFloat(self.percentage/100)
            
            let r = (1 - progress) * oldColor.red + progress * 0
            let g = (1 - progress) * oldColor.green + progress * 255
            let b = (1 - progress) * oldColor.blue + progress * 0

            self.progressView.backgroundColor = UIColor(displayP3Red: r, green: g, blue: b, alpha: 1)
            self.layoutIfNeeded()
        }
    }
}

// MARK: Happiness Notifications

func schedule50Notif() {
    print("Schedule 50")
    let notificationContent = UNMutableNotificationContent()
    notificationContent.title = "\(petNameValue ?? "Your pet") misses you!"
    notificationContent.body = "Click here to visit \(petNameValue ?? "your pet") now!"
    // set up the notification to trigger after a delay of "seconds"
    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 8, repeats: false)
    
    // set up a request to tell iOS to submit the notification with that trigger
    let request = UNNotificationRequest(identifier: "notif", content: notificationContent, trigger: notificationTrigger)
    
    //schedule notification
    notificationCenter.add(request) { (error) in
        if error != nil {
            // Handle any errors.
            print("50% notif error: \(error.debugDescription)")
        }
    }
}

func schedule25Notif() {
    let notificationContent = UNMutableNotificationContent()
    notificationContent.title = "\(petNameValue ?? "Your pet") needs attention!"
    notificationContent.body = "Click here to check on \(petNameValue ?? "your pet")"
    // set up the notification to trigger after a delay of "seconds"
    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 8, repeats: false)
    
    // set up a request to tell iOS to submit the notification with that trigger
    let request = UNNotificationRequest(identifier: "notif", content: notificationContent, trigger: notificationTrigger)
    
    //schedule notification
    notificationCenter.add(request) { (error) in
        if error != nil {
            // Handle any errors.
            print("25% notif error: \(error.debugDescription)")
        }
    }
}
