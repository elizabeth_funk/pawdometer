//
//  DashboardViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import HealthKit
import FirebaseCore
import FirebaseFirestore
import FirebaseDatabase

class DashboardViewController: UIViewController {
    
    let healthStore = HKHealthStore()
    var stepCount = ""
    
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var flightsClimbed: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var oneDay: UILabel!
    @IBOutlet weak var threeDays: UILabel!
    @IBOutlet weak var fiveDays: UILabel!
    
    @IBOutlet weak var caloriesBurned: UILabel!
    @IBOutlet weak var streakLabel: UILabel!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleLabel.alpha = 0.0
        healthData()
        labels()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        healthData()
        labels()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
            options: [],
            animations: {
                self.titleLabel.alpha = 1.0
        })
    }
    
    func updateStreakAndMoney() {
        var newMoney = ""
        var newStreak = ""

        let user = Auth.auth().currentUser

        if let user = user {
            let email = user.email

            db.collection("users").whereField("email_address", isEqualTo: email!)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {

                        for document in querySnapshot!.documents {

                            newMoney = (document.get("money") as? String)!
                            newStreak = (document.get("streak") as? String)!
                            streak = newStreak
                            moneyLeftValue = newMoney

                        }
                    }
            }
        }
    }
    
    
    func getStepsCount(forSpecificDate:Date, completion: @escaping (Double) -> Void) {
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let (start, end) = self.getWholeDate(date: forSpecificDate)

        let predicate = HKQuery.predicateForSamples(withStart: start, end: end, options: .strictStartDate)

        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(0.0)
                return
            }
            completion(sum.doubleValue(for: HKUnit.count()))
        }

        healthStore.execute(query)
    }

    func getWholeDate(date : Date) -> (startDate:Date, endDate: Date) {
        var startDate = date
        var length = TimeInterval()
        _ = Calendar.current.dateInterval(of: .day, start: &startDate, interval: &length, for: startDate)
        let endDate:Date = startDate.addingTimeInterval(length)
        return (startDate,endDate)
    }
    
    
    private func stepsToCalories() {
        
        let calPerMile = 0.57 * Double(weightValue!)
        let calPerStep = calPerMile / 2100.0
        
        if stepCount != "" {
            let numSteps = Double(stepCount)!
            let totalCalories = Int(numSteps * calPerStep)
            caloriesBurned.text = "\(String(totalCalories)) \n cals"
        } else {
            caloriesBurned.text = "0 \n cals"
        }
    }
    
    private func stepStreak() {
        let lastDate = Date(timeIntervalSince1970: TimeInterval(lastTimeChecked!.seconds))
        let inYesterday = Calendar.current.isDateInYesterday(lastDate)
        
        
        if inYesterday {
            let user = Auth.auth().currentUser
            if stepCount != "" {
                
                self.getStepsCount(forSpecificDate: lastDate) { (steps) in
                    
                    let currentMoney = Int(moneyLeftValue)!
                    var newMoneyAmountString = ""
                    var updatedStreak = ""
                    
                    if steps == 0.0 {
                        newMoneyAmountString = String(currentMoney)
                        updatedStreak = "0"
                    }
                    else {
                        DispatchQueue.main.async {
                            var addPointsDouble = steps / 1000.0
                            addPointsDouble.round(.down)
                            let addPointsInt = Int(addPointsDouble)
                            let newMoneyAmountInt = currentMoney + addPointsInt
                            newMoneyAmountString = String(newMoneyAmountInt)
                            
                            
                            if Int(steps) >= stepGoalValue! {
                                var currStreak = Int(streak)!
                                currStreak += 1
                                updatedStreak = String(currStreak)
                            } else {
                                updatedStreak = "0"
                            }
                            
                            streak = updatedStreak
                            moneyLeftValue = newMoneyAmountString
                            
                            if let user = user {
                                let email = user.email

                                db.collection("users").whereField("email_address", isEqualTo: email!)
                                    .getDocuments() { (querySnapshot, err) in
                                        if let err = err {
                                            print("Error getting documents: \(err)")
                                        } else {
                                            for document in querySnapshot!.documents {
                                                
                                                db.collection("users").document(document.documentID).updateData([
                                                    "streak": updatedStreak,
                                                    "money": newMoneyAmountString
                                                ])
                                            }
                                        }
                                }
                            }
                            
                            self.updateStreakAndMoney()
                        }
                    }
                }
            }
            
            if let user = user {
                let email = user.email

                db.collection("users").whereField("email_address", isEqualTo: email!)
                    .getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            for document in querySnapshot!.documents {
                                
                                db.collection("users").document(document.documentID).updateData([
                                    "last_time_checked": Date.init()
                                ])
                            }
                        }
                }
            }
            lastTimeChecked = Firebase.Timestamp()
            
        } 
    }
    
    private func healthData() {
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!, HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.flightsClimbed)!, HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)! ]
         
        // Check for Authorization
        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (success, error) in
         
            if (success) {
         
                // Authorization Successful
                self.getSteps { (result) in
         
                    DispatchQueue.main.async {
                        self.stepCount = String(Int(result))
                        self.stepsLabel.text = "\(self.stepCount) \n steps"
                        self.stepsToCalories()
                        self.stepStreak()
                        self.updateStreakAndMoney()
                        DispatchQueue.main.async {
                            if streak! == "1" {
                                self.streakLabel.text = "\(streak!) \n day"
                            } else {
                                self.streakLabel.text = "\(streak!) \n days"
                            }
                        }
                        
                    }
                }
                
                self.getFlights { (result) in
                
                    DispatchQueue.main.async {
                        let flights = String(Int(result))
                        self.flightsClimbed.text = "\(flights) \nflights"
                    }
                }
                
                self.getDistance { (result) in
                
                    DispatchQueue.main.async {
                        let distance = String(Double(result))
                        self.distance.text = "\(distance) \nmiles"
                    }
                }
            } // end if
         
        } // end of checking authorization
        
    }
    
    private func labels() {

        let height = Double(((heightFeetValue! * 12) + heightInchesValue!)) * 2.54
        let weightKG = Double(weightValue!) / 2.2046

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d,yyyy"
        let date = dateFormatter.date(from: birthdayValue!)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: date!)
        let year = components.year
        let currentDate = Date()
        let currentYear = calendar.component(.year, from: currentDate)
        let age = currentYear - year!
        
        var baseCalories = 0.0
        if(genderValue == "Male") {
            let stepOne = (13.397 * weightKG)
            let stepTwo = (4.799 * height)
            let stepThree = (5.677 * Double(age))
            baseCalories = (stepOne + stepTwo - stepThree) + 88.362
        
        } else {
            let stepOne = (9.247 * weightKG)
            let stepTwo = (3.098 * height)
            let stepThree = (4.330 * Double(age))
            baseCalories = (stepOne + stepTwo - stepThree) + 447.593
        }
        
        let one = Int(baseCalories * 1.3)
        self.oneDay.text = String(one)
        
        let three = Int(baseCalories * 1.5)
        self.threeDays.text = String(three)
        
        let five = Int(baseCalories * 1.6)
        self.fiveDays.text = String(five)
    }
    
    
    func getSteps(completion: @escaping (Double)-> Void) {
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
         
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfDay,
        intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
                var resultCount = 0.0
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.count())
                } // end if
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        query.statisticsUpdateHandler = {
         
            query, statistics, statisticsCollection, error in
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func getFlights(completion: @escaping (Double)-> Void) {
        let flightQuantityType = HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!
         
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(
        quantityType: flightQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfDay,
        intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
                var resultCount = 0.0
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.count())
                } // end if
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        query.statisticsUpdateHandler = {
         
            query, statistics, statisticsCollection, error in
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func getDistance(completion: @escaping (Double)-> Void) {
        let distanceQuantityType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
         
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(
        quantityType: distanceQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfDay,
        intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
                var resultCount = 0.0
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.mile())
                } // end if
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        query.statisticsUpdateHandler = {
         
            query, statistics, statisticsCollection, error in
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.mile())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        guard sender.view != nil else { return }
             
        if sender.state == .ended {      // Move the view down and to the right when tapped.
            let alert = UIAlertController(title: "What are maintenance calories?", message: "Your body requires a certain amount of energy to perform basic functions such as muscle movement and cell growth. That amount of energy is known as maintenance calories. The different BMR ranges that are displayed are based on if you exercise 1-3 times a week, 3-5 times a week, or 5-7 times a week.", preferredStyle: UIAlertController.Style.alert)
           // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
           // show the alert
           self.present(alert, animated: true, completion: nil)
        }
    }
}
