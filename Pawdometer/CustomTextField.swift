//
//  CustomTextField.swift
//  Pawdometer
//
//  Created by Tracy on 4/5/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit

class CustomTextField : UITextField, UITextFieldDelegate {

    override var tintColor: UIColor! {
        didSet {
            setNeedsDisplay()
        }
    }

    override func draw(_ rect: CGRect) {

        let startingPoint = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint = CGPoint(x: rect.maxX, y: rect.maxY)

        let path = UIBezierPath()

        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 1.5

        tintColor.setStroke()

        path.stroke()
    }
}
