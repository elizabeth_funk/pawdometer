//
//  CreateAccountViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore


// To create the Picker objects for the Birthday TextField
extension UITextField {

   func addInputViewDatePicker(target: Any, selector: Selector) {

    let screenWidth = UIScreen.main.bounds.width

    //Add DatePicker as inputView
    let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
    datePicker.datePickerMode = .date
    self.inputView = datePicker

    //Add Tool Bar as input AccessoryView
    let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
    let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
    toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)

    self.inputAccessoryView = toolBar
    }

    @objc func cancelPressed() {
        self.resignFirstResponder()
    }
}


class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    
    var db: Firestore!
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var petName: UITextField!
    
    @IBOutlet weak var createAccountEmail: UITextField!
    @IBOutlet weak var createAccountPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var birthday: UITextField!
    
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var heightInches: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var stepGoal: UITextField!
    @IBOutlet weak var createAccount: UIButton!
    
    var birthdayFilled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createAccount.isEnabled = false
        [name, petName, createAccountEmail, createAccountPassword, confirmPassword, birthday, height, heightInches, weight, stepGoal].forEach({ $0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
        
        // [START setup]
        let settings = FirestoreSettings()

        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        // Format the birthday DatePicker
        birthday.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed))
        
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.count == 1 {
            if textField.text?.first == " " {
                textField.text = ""
                return
            }
        }
        
        guard
            let one = name.text, !one.isEmpty,
            let two = petName.text, !two.isEmpty,
            let three = createAccountEmail.text, !three.isEmpty,
            let four = createAccountPassword.text, !four.isEmpty,
            let five = confirmPassword.text, !five.isEmpty,
            birthdayFilled == true,
            let seven = height.text, !seven.isEmpty,
            let eight = heightInches.text, !eight.isEmpty,
            let nine = weight.text, !nine.isEmpty,
            let ten = stepGoal.text, !ten.isEmpty
        else {
            createAccount.isEnabled = false
            return
        }
        createAccount.isEnabled = true
    }
    
    
    // Done button pressed for birthday TextField picker
    @objc func doneButtonPressed() {
        
       if let datePicker = self.birthday.inputView as? UIDatePicker {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .medium
           self.birthday.text = dateFormatter.string(from: datePicker.date)
       }
       self.birthday.resignFirstResponder()
       birthdayFilled = true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case name:
            createAccountEmail.becomeFirstResponder()
        case createAccountEmail:
            createAccountPassword.becomeFirstResponder()
        case createAccountPassword:
            confirmPassword.becomeFirstResponder()
        case confirmPassword:
            petName.becomeFirstResponder()
        case petName:
            birthday.becomeFirstResponder()
        case birthday:
            height.becomeFirstResponder()
        case height:
            heightInches.becomeFirstResponder()
        case heightInches:
            weight.becomeFirstResponder()
        case weight:
            stepGoal.becomeFirstResponder()
        default:
            name.resignFirstResponder()
        }

        return true
    }
    
    @IBAction func createAccountPressed(_ sender: Any) {
        if createAccountPassword.text != confirmPassword.text {
            let alert = UIAlertController(
                title: "Password don't match.",
                message: "Ensure that passwords match.",
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK",style:.default))
            self.present(alert, animated:true, completion: nil)
        } else {
            Auth.auth().createUser(withEmail: createAccountEmail.text!, password: createAccountPassword.text!){
                (user, error) in
                if let error = error, user == nil {
                    let alert = UIAlertController(
                        title: "Error",
                        message: error.localizedDescription,
                        preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"OK",style:.default))
                    self.present(alert, animated:true, completion: nil)
                } else {
                    self.addAccountInfo()
                    Auth.auth().signIn(withEmail: self.createAccountEmail.text!, password: self.createAccountPassword.text!)
                    let vc = self.view?.window?.rootViewController
                    vc?.performSegue(withIdentifier: "successfulLogin", sender: nil)
                }
            }
        }
    }
    
    private func checkGender() -> String {
        if maleButton.isSelected {
            return "Male"
        } else {
            return "Female"
        }
    }
    
    private func addAccountInfo() {
        
        // Saving values to app-wide variables after user has created account for the first time
        heightFeetValue = Int(height.text!)
        heightInchesValue = Int(heightInches.text!)
        weightValue = Int(weight.text!)
        genderValue = checkGender()
        birthdayValue = birthday.text!
        nameValue = name.text!
        stepGoalValue = Int(stepGoal.text!)
        petNameValue = petName.text!
        moneyLeftValue = "0"
        streak = "0"
        lastTimeChecked = Firebase.Timestamp.init()
        
        // Add a new document with a generated ID
        var ref: DocumentReference? = nil
        ref = db.collection("users").addDocument(data: [
            "email_address": createAccountEmail.text!,
            "password": createAccountPassword.text!,
            "name": name.text!,
            "pet_name": petName.text!,
            "birthday": birthday.text!,
            "height_feet": height.text!,
            "height_inches": heightInches.text!,
            "weight": weight.text!,
            "step_goal": stepGoal.text!,
            "gender": checkGender(),
            "inventory": [String: [Int]](),
            "money": "0",
            "streak": "0",
            "last_time_checked": Firebase.Timestamp.init(),
            "notifs": true
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
    @IBAction func genderChecked(_ sender: UIButton) {
        let buttonArray = [maleButton,femaleButton]
        buttonArray.forEach{
            $0?.isSelected = false
        }
        sender.isSelected = true
    }
    
    
    @IBAction func infoButtonSelected(_ sender: UIButton) {
        let alert = UIAlertController(title: "Why do we need your gender?", message: "We use your gender to determine the number of calories your body needs based on the number of days you are active.", preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
         alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    // Code to dismiss keyboard when user clicks on background
    func textFieldShouldClear(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
                
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
