//
//  PetStoreViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseDatabase

class PetStoreViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moneyLeft: UILabel!
    @IBOutlet weak var petStoreTitle: UILabel!
    
    var imageData = ["dog-food", "bone", "tennis", "hat", "cowboy-hat", "party-hat", "eiffel-tower", "statue-of-liberty", "beach"]
    var priceData = ["1", "3", "5", "7", "7", "7", "10", "10", "10"]
    var imageCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // [START setup]
        let settings = FirestoreSettings()

        Firestore.firestore().settings = settings
        // [END setup]
        
        // Do any additional setup after loading the view.
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // Append paw point coin image to after money left
        let money = NSMutableAttributedString(string: "\(moneyLeftValue!) ")
        let coin = NSTextAttachment()
        coin.image = resizeImage(image: UIImage(named: "PawCoin.png")!, targetSize: CGSize(width: 23.0, height: 23.0))
        
        let coinCurrency = NSAttributedString(attachment: coin)
        money.append(coinCurrency)
        moneyLeft.attributedText = money
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.petStoreTitle.alpha = 0.0
        self.collectionView.alpha = 0.0
        self.collectionView.center.y += view.bounds.width

    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.25,
                   delay: 0.0,
        options: [],
        animations: {
            self.collectionView.alpha = 1.0
            self.collectionView.center.y -= self.view.bounds.width
        })
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
            options: [],
            animations: {
                self.petStoreTitle.alpha = 1.0
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Be sure to cast the following to your custom Cell class using “as!”
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "petStoreCellIdentifier", for: indexPath) as! PetStoreCollectionViewCell
        
        let curImageName = self.imageData[self.imageCounter]
        let curImagePrice = NSMutableAttributedString(string: "\(self.priceData[self.imageCounter]) ")

        // Append paw point coin image to after price
        let coin = NSTextAttachment()
        coin.image = resizeImage(image: UIImage(named: "PawCoin.png")!, targetSize: CGSize(width: 17.0, height: 17.0))
        
        let coinCurrency = NSAttributedString(attachment: coin)
        curImagePrice.append(coinCurrency)

        self.imageCounter += 1
        if self.imageCounter >= self.imageData.count {
            self.imageCounter = 0
        }
    
        // reference to my variable "image" in MyImageCell.swift
        
        let newImage = resizeImage(image: UIImage(named: curImageName)!, targetSize: CGSize(width: 80.0, height: 80.0))
        
        cell.itemImage.image = newImage
        cell.itemPrice.attributedText = curImagePrice
        
        cell.layer.borderWidth = 3
        
        if cell.isSelected {
            //put border logic
            cell.layer.borderColor = UIColor.systemGray.cgColor
            cell.contentView.backgroundColor = UIColor(red: 199/256, green: 199/256, blue: 204/256, alpha: 1)

        }else {
            // remove border
            cell.layer.borderColor = UIColor(red: 199/256, green: 199/256, blue: 204/256, alpha: 1).cgColor
            cell.contentView.backgroundColor = UIColor.white

        }
        
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! UICollectionViewCell
        cell.layer.borderColor = UIColor.systemGray.cgColor
        cell.contentView.backgroundColor = UIColor(red: 199/256, green: 199/256, blue: 204/256, alpha: 1)
        cell.isSelected = true
    
        // check amount of money and check price of item clicked - if not enough money - create alert saying not enough money
        
        var moneyLeftInt:Int = Int(moneyLeftValue)!
        let price:Int = Int(priceData[indexPath.row])!
        
        if (price > moneyLeftInt) {
            let insufficientMoneyAlert = UIAlertController(title: "Insufficient Money", message: "You do not have enough money to purchase this item.", preferredStyle: UIAlertController.Style.alert)
            insufficientMoneyAlert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil))
            present(insufficientMoneyAlert, animated: true, completion: nil)
        }
        
        else {
            let confirmPurchaseAlert = UIAlertController(title: "Confirm Purchase", message: "Are you sure you want to purchase this item?", preferredStyle: UIAlertController.Style.alert)
            
            confirmPurchaseAlert.addAction(UIAlertAction(title: "Buy", style: .default, handler: { (action: UIAlertAction!) in
                
                let itemString = self.imageData[indexPath.row]
                
                moneyLeftInt -= price
                moneyLeftValue = String(moneyLeftInt)
                // get current logged in user
                // update changes in Firestore
                
                let user = Auth.auth().currentUser
                
                if let user = user {
                    let email = user.email

                    db.collection("users").whereField("email_address", isEqualTo: email!)
                        .getDocuments() { (querySnapshot, err) in
                            if let err = err {
                                print("Error getting documents: \(err)")
                            } else {
                                for document in querySnapshot!.documents {
                                    
                                    var inventoryDict = document.get("inventory") as! [String: [Int]]
                                    
                                    if (itemString == "eiffel-tower" || itemString == "beach" || itemString == "statue-of-liberty") {
                                        if (inventoryDict[itemString] == nil) {
                                            inventoryDict[itemString] = [0, 0]
                                        }
                                        let count = inventoryDict[itemString]![0] + 1
                                        inventoryDict[itemString] = [count, 0]
                                        
                                    } else {
                                        if (inventoryDict[itemString] == nil) {
                                            inventoryDict[itemString] = [0, 1]
                                        }
                                        let count = inventoryDict[itemString]![0] + 1
                                        inventoryDict[itemString] = [count, 1]
                                    }
                                    db.collection("users").document(document.documentID).updateData([
                                        "inventory": inventoryDict,
                                        "money": moneyLeftValue!
                                    ])
                                }
                            }
                    }
                    // Append paw point coin image to after money left
                    let money = NSMutableAttributedString(string: "\(moneyLeftValue!) ")
                    let coin = NSTextAttachment()
                    coin.image = self.resizeImage(image: UIImage(named: "PawCoin.png")!, targetSize: CGSize(width: 23.0, height: 23.0))
                    
                    let coinCurrency = NSAttributedString(attachment: coin)
                    money.append(coinCurrency)
                    self.moneyLeft.attributedText = money
                }
            }))

            confirmPurchaseAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            present(confirmPurchaseAlert, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor(red: 199/256, green: 199/256, blue: 204/256, alpha: 1).cgColor
        cell?.contentView.backgroundColor = UIColor.white
        cell?.isSelected = false
    }
}
