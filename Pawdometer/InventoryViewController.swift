//
//  InventoryViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseDatabase

class InventoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var db: Firestore!
    var delegate: Inventory?
    var items: [String] = []
    var inventory: [String:[Int]] = [:]
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()

        collectionView.dataSource = self
        collectionView.delegate = self
        
        // Get current logged in user
        let user = Auth.auth().currentUser
        
        if let user = user {
            let email = user.email

            self.db.collection("users").whereField("email_address", isEqualTo: email!)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            if (document.get("inventory") != nil){
                                self.inventory = document.get("inventory") as! [String:[Int]]
                                self.items = Array(self.inventory.keys)
                            }
                        }
                        self.collectionView.reloadData()
                    }
            }
        }
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuseIdentifier", for: indexPath) as! InventoryCollectionViewCell
        
        let item = items[indexPath.row]
        cell.itemCount.text = "x \(inventory[item]![0])"
        let newImage = resizeImage(image: UIImage(named: item)!, targetSize: CGSize(width: 80.0, height: 80.0))
        
        cell.itemImage.image = newImage
        cell.layer.borderWidth = 3
        
        if cell.isSelected {
            //put border logic
            cell.layer.borderColor = UIColor.black.cgColor
            cell.contentView.backgroundColor = UIColor(red: 68/256, green: 45/256, blue: 39/256, alpha: 1)

        }else {
            // remove border
            cell.layer.borderColor = UIColor(red: 68/256, green: 45/256, blue: 39/256, alpha: 1).cgColor
            cell.contentView.backgroundColor = UIColor(red: 161/256, green: 119/256, blue: 103/256, alpha: 1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! InventoryCollectionViewCell
        cell.layer.borderColor = UIColor.black.cgColor
        cell.contentView.backgroundColor = UIColor(red: 68/256, green: 45/256, blue: 39/256, alpha: 1)
        cell.isSelected = true
        
        let confirmItemUseAlert = UIAlertController(title: "Use Item", message: "Are you sure you want to use this item?", preferredStyle: UIAlertController.Style.alert)
        
        confirmItemUseAlert.addAction(UIAlertAction(title: "Use", style: .default, handler: { (action: UIAlertAction!) in
            
            let item = self.items[indexPath.row]
            let type = self.inventory[item]?[1]
            if (type == 1) {
                self.delegate?.showItem(itemName: item, x: nil, y: nil, rotation: nil)
            } else {
                self.delegate?.changeBackground(backgroundName: item)
            }
            
            // Decrease count of item after it is used
            let currItemCount = self.inventory[item]?[0]
            if (currItemCount == 1) {
                self.inventory[item] = nil
                cell.itemCount.text = "x 0"
            } else {
                self.inventory[item]?[0] = currItemCount! - 1
                cell.itemCount.text = "x \(self.inventory[item]![0])"
            }
                            
            let user = Auth.auth().currentUser
            
            if let user = user {
                let email = user.email

                self.db.collection("users").whereField("email_address", isEqualTo: email!)
                    .getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            for document in querySnapshot!.documents {
                                
                                self.db.collection("users").document(document.documentID).updateData([
                                    "inventory": self.inventory
                                ])
                            }
                        }
                }
            }
        }))

        confirmItemUseAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        present(confirmItemUseAlert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor(red: 68/256, green: 45/256, blue: 39/256, alpha: 1).cgColor
        cell?.contentView.backgroundColor = UIColor(red: 161/256, green: 119/256, blue: 103/256, alpha: 1)
        cell?.isSelected = false
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    @IBAction func moreInfoTapped(_ sender: Any) {
        let alert = UIAlertController(title: "What is this?", message: "This is your inventory. All your purchased items and backgrounds go here. You can tap on any one of them to use it for your pet.", preferredStyle: UIAlertController.Style.alert)
         alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
