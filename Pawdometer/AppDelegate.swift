//
//  AppDelegate.swift
//  Pawdometer
//
//  Created by Kimberly Hwang on 3/7/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseDatabase
import UserNotifications
import BackgroundTasks

// Saving all user settings if the user is already logged
var heightFeetValue: Int!
var heightInchesValue: Int!
var weightValue: Int!
var genderValue: String!
var birthdayValue: String!
var nameValue: String!
var stepGoalValue: Int!
var petNameValue: String!
var moneyLeftValue: String!
var db: Firestore!
var streak: String!
var lastTimeChecked: Firebase.Timestamp!
var notificationsValue: Bool!

let notificationCenter = UNUserNotificationCenter.current()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        print(UserDefaults.standard.bool(forKey: "isLoggedIn"))
        let rootController: UIViewController!
        
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            
            rootController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
          
        } else {
            rootController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarVC") as! TabBarController
      
        }
        self.window?.rootViewController = rootController
        self.window?.makeKeyAndVisible()
     
        let settings = FirestoreSettings()

        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        let user = Auth.auth().currentUser
        if let user = user {
            let email = user.email
        
            db.collection("users").whereField("email_address", isEqualTo: email!)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            heightInchesValue = Int((document.get("height_inches") as? String)!)
                            weightValue = Int((document.get("weight") as? String)!)
                            birthdayValue = document.get("birthday") as? String
                            heightFeetValue = Int((document.get("height_feet") as? String)!)
                            genderValue = document.get("gender") as? String
                            nameValue = document.get("name") as? String
                            petNameValue = document.get("pet_name") as? String
                            stepGoalValue = Int((document.get("step_goal") as? String)!)
                            moneyLeftValue = document.get("money") as? String
                            streak = document.get("streak") as? String
                            lastTimeChecked = document.get("last_time_checked") as? Firebase.Timestamp
                            notificationsValue = document.get("notifs") as? Bool
                        }
                    }
            }
        }
        
        //setup recurrring daily notification at 11am
        notificationCenter.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted && (notificationsValue ?? true) {
                self.scheduleDailyNotif();
            } else {
                print("Permission Denied: \(error.debugDescription)")
            }
        }
        return true
    }
    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: Daily Notification
    func scheduleDailyNotif() {
        //content
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "\(petNameValue ?? "Your pet") wants to go for a walk!"
        notificationContent.body = "Click here to check on \(petNameValue ?? "your pet")"
        //time
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        dateComponents.hour = 11
        let dailyNotifTrigger = UNCalendarNotificationTrigger(
        dateMatching: dateComponents, repeats: true)
        
        let dailyNotifuuidString = UUID().uuidString
        let dailyNotifRequest = UNNotificationRequest(identifier: dailyNotifuuidString,
                    content: notificationContent, trigger: dailyNotifTrigger)
        //schedule notification
        notificationCenter.add(dailyNotifRequest) { (error) in
            if error != nil {
                // Handle any errors.
                print("Daily notif error: \(error.debugDescription)")
            }
        }
    }
    

    

}

