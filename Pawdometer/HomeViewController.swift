//
//  HomeViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import SpriteKit
import HealthKit

protocol Inventory {
    func showItem(itemName : String, x : Int!, y : Int!, rotation: Double!)
    func changeBackground(backgroundName : String)
}

public var defaultItemCount = 0

class HomeViewController: UIViewController, UIPopoverPresentationControllerDelegate, Inventory {
    
    let popoverSegueIdentifier = "popoverSegue"
    let progressBar: ProgressBar
    var backgroundView : UIView
    let healthStore = HKHealthStore()
    var currPercent: Int = 0
    var stepCount: Double = 0 {
        didSet {
            if stepGoalValue != nil {
                if Int(stepCount) >= stepGoalValue! && Int(oldValue) < stepGoalValue! && (notificationsValue ?? true){
                    scheduleFullStepsNotif()
                } else if Int(stepCount) >= stepGoalValue!/2  && Int(oldValue) < stepGoalValue!/2 && (notificationsValue ?? true){
                    scheduleHalfStepsNotif()
                }
            }
        }
    }
   
    @IBOutlet weak var dogHead: UIImageView!
    @IBOutlet weak var dogTail: UIImageView!
    @IBOutlet weak var flooring: UIImageView!
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var percentChange: UILabel!
    @IBOutlet weak var inventoryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Add the progress/happiness bar programatically to view
        self.view.addSubview(progressBar)
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 60).isActive = true
        progressBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        progressBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        getStepCount()
        decreaseBarOverTime()
        petName.text = petNameValue ?? ""
        checkUserDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        progressBar = ProgressBar()
        backgroundView = UIView()
        super.init(coder: aDecoder)
    }
    
    // Removes progress/happiness of the pet over time
    private func decreaseBarOverTime() {
        if (progressBar.percentage > 0) {
            progressBar.percentage -= 1 // Decrease by 1% every 15 minutes
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 900.0) { [weak self] in
            self?.decreaseBarOverTime()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.progressBar.alpha = 0.0
        self.petName.center.x += self.view.bounds.width
        self.inventoryButton.center.x += self.view.bounds.width
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkDefaultBackground()
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.progressBar.alpha = 1.0
            self.petName.center.x -= self.view.bounds.width
            self.inventoryButton.center.x -= self.view.bounds.width
        })
        
        // Update step count
        getStepCount()

        // Animations for the dog head and tail
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: dogTail.center.x - 10, y: dogTail.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: dogTail.center.x + 10, y: dogTail.center.y))
        
        let animation2 = CABasicAnimation(keyPath: "position")
        animation2.duration = 3
        animation2.repeatCount = 10
        animation2.autoreverses = true
        animation2.fromValue = NSValue(cgPoint: CGPoint(x: dogHead.center.x - 5, y: dogHead.center.y))
        animation2.toValue = NSValue(cgPoint: CGPoint(x: dogHead.center.x + 5, y: dogHead.center.y))

        dogTail.layer.add(animation, forKey: "position")
        dogHead.layer.add(animation2, forKey: "position")
        
        // Set pet name and progress bar
        let percent = 100.0 * stepCount / Double(stepGoalValue ?? 10000)
        if (percent > 100) {
            setBar(percent: 100.00)
            currPercent = 100
        } else if (Double(currPercent) < percent){ // Progress with items/backgrounds
            setBar(percent: Double(Int(percent)))
            currPercent = Int(percent)
        }
        petName.text = petNameValue ?? ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == popoverSegueIdentifier,
            let inventoryVC = segue.destination as? InventoryViewController{
            inventoryVC.delegate = self
            
            inventoryVC.modalPresentationStyle = .popover
            inventoryVC.popoverPresentationController?.delegate = self as UIPopoverPresentationControllerDelegate
        }
    }
    
    // Displays an pet item in "use" from the inventory
    func showItem(itemName: String, x: Int!, y: Int!, rotation: Double!) {
        let newImage = resizeImage(image: UIImage(named: itemName)!, targetSize: CGSize(width: 150.0, height: 150.0))
        
        let newImageView = ItemView(image: newImage, rootVC: self, index: defaultItemCount)
        if let xPos = x, let yPos = y {
            // Restore item position from last use
            newImageView.frame = CGRect(x: xPos, y: yPos, width: 150, height: 150)
        } else {
            // Randomly place item in front of pet
            newImageView.frame = CGRect(x: Int(arc4random_uniform(265)) + 5, y: Int(arc4random_uniform(100)) + 550, width: 150, height: 150)
        }
        // Rotate image if it was stored that way from saved defaults
        if let rot = rotation {
            print("image rotated \(rot) degrees")
            newImageView.transform = CGAffineTransform(rotationAngle: CGFloat(rot))
        }
        
        self.view.addSubview(newImageView)
        self.view.bringSubviewToFront(newImageView)
        
        // Store state of items selected for the pet screen
        let defaults = UserDefaults.standard
        defaults.set(itemName, forKey: String(defaultItemCount))
       
        defaults.set(newImageView.frame.origin.x, forKey: String(defaultItemCount) + "x")
        defaults.set(newImageView.frame.origin.y, forKey: String(defaultItemCount) + "y")
        
        defaultItemCount += 1
        defaults.set(defaultItemCount, forKey: "itemCount")
        
        increaseBar(percent: 5)
    }
    
    // Changes the background with a in "use" background from the inventory
    func changeBackground(backgroundName: String) {
        backgroundView.removeFromSuperview()
        
        let newImage = resizeImage(image: UIImage(named: backgroundName)!, targetSize: CGSize(width: 400.0, height: 400.0))
        let otherView = UIView(frame: CGRect(x: 7, y: 145, width: 400, height: 400))
        otherView.backgroundColor = UIColor(patternImage: newImage)
        
        self.view.addSubview(otherView)
        self.view.sendSubviewToBack(otherView)
        
        // Store background selected
        let defaults = UserDefaults.standard
        defaults.set(backgroundName, forKey: "background")
        
        backgroundView = otherView
        increaseBar(percent: 15)
    }
    
    // Increases the progress/happiness bar by given percent with animations
    func increaseBar(percent: Double) {
        if (progressBar.percentage == 100) {
            return
        }
        if (progressBar.percentage + percent <= 100) {
            progressBar.percentage += percent
        } else { // Fill bar completely
            progressBar.percentage += 100 - progressBar.percentage
        }
        percentChange.text = "+\(Int(percent))%"
        UIView.animate(withDuration: 1.5, animations: {
            self.percentChange.alpha = 1
        }) { (completed) in
            UIView.animate(withDuration: 1.5, animations: {
            self.percentChange.alpha = 0
           })
        }
    }
    
    func setBar(percent: Double) {
        let currPercent = progressBar.percentage
        let diff = percent - currPercent
        if (diff != 0) {
            progressBar.percentage += diff
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    func getStepCount() {
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!]
         
        // Check for Authorization
        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (success, error) in
         
            if (success) {
         
                // Authorization Successful
                self.getSteps { (result) in
         
                    DispatchQueue.main.async {
                        self.stepCount = (Double(Int(result)))
                    }
                }
            } // end if
         
        } // end of checking authorization
    }
    
    func getSteps(completion: @escaping (Double)-> Void) {
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
         
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(
        quantityType: stepsQuantityType,
        quantitySamplePredicate: nil,
        options: [.cumulativeSum],
        anchorDate: startOfDay,
        intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
                var resultCount = 0.0
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.count())
                } // end if
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        query.statisticsUpdateHandler = {
         
            query, statistics, statisticsCollection, error in
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        healthStore.execute(query)
    }
    
    func checkDefaultBackground() {
        let defaults = UserDefaults.standard
        if let savedBackground = defaults.string(forKey: "background") {
            changeBackground(backgroundName: savedBackground)
        } else {
            backgroundView.removeFromSuperview()
        }
    }
    
    func checkUserDefaults() {
        let defaults = UserDefaults.standard
        if let savedBackground = defaults.string(forKey: "background") {
            changeBackground(backgroundName: savedBackground)
        } else {
            backgroundView.removeFromSuperview()
        }
        let itemCount = defaults.integer(forKey: "itemCount")
        for i in 0..<itemCount {
            if let itemName = defaults.string(forKey: String(i)) {
                
                let xPos = defaults.integer(forKey: String(i) + "x")
                let yPos = defaults.integer(forKey: String(i) + "y")
                let rotation = defaults.double(forKey: String(i) + "Rotation")

                showItem(itemName: itemName, x: xPos, y: yPos, rotation: rotation)
            }
        }
    }
}

// MARK: Step Notifications
func scheduleHalfStepsNotif() {
    let notificationContent = UNMutableNotificationContent()
    notificationContent.title = "You and \(petNameValue ?? "your pet") are halfway to your step goal!"
    notificationContent.body = "Keep going to reach your goal"
    // set up the notification to trigger after a delay of "seconds"
    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 8, repeats: false)
    
    // set up a request to tell iOS to submit the notification with that trigger
    let request = UNNotificationRequest(identifier: "notif", content: notificationContent, trigger: notificationTrigger)
    
    //schedule notification
    notificationCenter.add(request) { (error) in
        if error != nil {
            // Handle any errors.
            print("Halfway to step goal notif error: \(error.debugDescription)")
        }
    }
}

func scheduleFullStepsNotif() {
    let notificationContent = UNMutableNotificationContent()
    notificationContent.title = "You and \(petNameValue ?? "your pet") reached your step goal!"
    notificationContent.body = "Congrats! Keep it up tomorrow :D"
    // set up the notification to trigger after a delay of "seconds"
    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 8, repeats: false)
    
    // set up a request to tell iOS to submit the notification with that trigger
    let request = UNNotificationRequest(identifier: "notif", content: notificationContent, trigger: notificationTrigger)
    
    //schedule notification
    notificationCenter.add(request) { (error) in
        if error != nil {
            // Handle any errors.
            print("Reached step goal notif error: \(error.debugDescription)")
        }
    }
}
