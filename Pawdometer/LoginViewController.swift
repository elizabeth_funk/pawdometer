//
//  LoginViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore

class LoginViewController: UIViewController {

    @IBOutlet weak var loginEmail: UITextField!
    @IBOutlet weak var loginPassword: UITextField!
    @IBOutlet weak var logo: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var newUserButton: UIButton!
    @IBOutlet weak var forgotPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // [START setup]
        let settings = FirestoreSettings()

        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        self.view.isHidden = true
        Auth.auth().addStateDidChangeListener() {
            auth, user in
            if user == nil {
            self.view.isHidden = false
            self.loginEmail.text = nil
            self.loginPassword.text = nil
            self.dismiss(animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: "successfulLogin", sender: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logo.alpha = 0.0
        self.loginButton.alpha = 0.0
        self.newUserButton.alpha = 0.0
        self.loginEmail.alpha = 0.0
        self.loginPassword.alpha = 0.0
        self.forgotPassword.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
         
        self.logo.center.y -= view.bounds.width
        self.loginButton.center.x -= self.view.bounds.width
        self.newUserButton.center.x += self.view.bounds.width
        self.loginEmail.alpha = 0.0
        self.loginPassword.alpha = 0.0
        self.forgotPassword.alpha = 0.0
        
        UIView.animate(withDuration: 1.0,
            delay: 0.5,
            usingSpringWithDamping: 0.2,
            initialSpringVelocity: 0.3,
            options: [],
            animations: {
                self.logo.alpha = 1.0
                self.logo.center.y += self.view.bounds.width
            })
        UIView.animate(withDuration: 1.5, delay: 1.0, options: [],
            animations: {
                self.loginEmail.alpha = 1.0
                self.loginPassword.alpha = 1.0
                self.forgotPassword.alpha = 1.0
            },
            completion: nil)
        UIView.animate(withDuration: 1.0, delay: 0.5,
            usingSpringWithDamping: 0.2,
            initialSpringVelocity: 0.3,
            options: [],
            animations: {
                self.loginButton.alpha = 1.0
                self.newUserButton.alpha = 1.0
                self.loginButton.center.x += self.view.bounds.width
                self.newUserButton.center.x -= self.view.bounds.width
            },
            completion: nil)
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let email = loginEmail.text,
            let password = loginPassword.text,
            email.count > 0,
            password.count > 0
        else {
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) {
            user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(
                    title: "Sign in failed",
                    message: error.localizedDescription,
                    preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK",style:.default))
                self.present(alert, animated:true, completion: nil)
            } else {
                let user = Auth.auth().currentUser
                print("first here")
                if let user = user {
                    let email = user.email
                
                    db.collection("users").whereField("email_address", isEqualTo: email!)
                        .getDocuments() { (querySnapshot, err) in
                            if let err = err {
                                print("Error getting documents: \(err)")
                            } else {
    
                                for document in querySnapshot!.documents {
                                    heightInchesValue = Int((document.get("height_inches") as? String)!)
                                    weightValue = Int((document.get("weight") as? String)!)
                                    birthdayValue = document.get("birthday") as? String
                                    heightFeetValue = Int((document.get("height_feet") as? String)!)
                                    genderValue = document.get("gender") as? String
                                    nameValue = document.get("name") as? String
                                    petNameValue = document.get("pet_name") as? String
                                    stepGoalValue = Int((document.get("step_goal") as? String)!)
                                    moneyLeftValue = document.get("money") as? String
                                    streak = document.get("streak") as? String
                                    lastTimeChecked = document.get("last_time_checked") as? Firebase.Timestamp
                                    notificationsValue = document.get("notifs") as? Bool
                                }
                            }
                    }
                }
                
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.synchronize()
                print("login")
                print(UserDefaults.standard.bool(forKey: "isLoggedIn"))
                //Switcher.updateRootVC()
                self.performSegue(withIdentifier: "successfulLogin", sender: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        loginEmail.text?.removeAll()
        loginPassword.text?.removeAll()
    }
    
    // Code to dismiss keyboard when user clicks on background
    func textFieldShouldClear(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
                
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        
        let controller = UIAlertController(
            title: "Forgot Password?",
            message: "",
            preferredStyle: .alert)
        controller.addAction(UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil))
        
        // Dynamically create a text field
        // addTextField takes as a parameter a function that creates a
        // textField object and adds the text field to an array called
        // "textFields".
        
        controller.addTextField(configurationHandler: {
            (textField:UITextField!) in
            textField.placeholder = "Enter your email address:"
        })
        controller.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: {
                (paramAction:UIAlertAction!) in
                if let textFieldArray = controller.textFields {
                    let textFields = textFieldArray as [UITextField]
                    let enteredText = textFields[0].text
                    
                    Auth.auth().sendPasswordReset(withEmail: enteredText!) { error in
                        print("sent email!")
                    }
                }
        }))
        present(controller, animated: true, completion: nil)
    }
}
