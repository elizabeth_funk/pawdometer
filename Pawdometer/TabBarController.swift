//
//  TabBarController.swift
//  Pawdometer
//
//  Created by Tracy on 3/19/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    @IBInspectable var defaultIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        let prominentTabBar = self.tabBar as! ProminentTabBar
        prominentTabBar.prominentButtonCallback = prominentTabTaped
        
        selectedIndex = defaultIndex
    }

    func prominentTabTaped() {
        selectedIndex = (tabBar.items?.count ?? 0)/2
    }

}
