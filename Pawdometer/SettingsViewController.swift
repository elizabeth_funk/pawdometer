//
//  SettingsViewController.swift
//  Pawdometer
//
//  Created by Ramya Prasad on 3/10/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseDatabase

class SettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var settingsTitle: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var petNameField: UITextField!
    
    @IBOutlet weak var heightFeetField: UITextField!
    @IBOutlet weak var heightInchesField: UITextField!
    
    @IBOutlet weak var stepGoalField: UITextField!
    @IBOutlet weak var birthdayField: UITextField!
    @IBOutlet weak var weightField: UITextField!
    @IBOutlet weak var notificationsField: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // [START setup]
        let settings = FirestoreSettings()

        Firestore.firestore().settings = settings
        // [END setup]
        
        // Format the birthday DatePicker
        birthdayField.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed))
        
        self.nameField.text = nameValue
        self.petNameField.text = petNameValue!
        self.heightFeetField.text = String(heightFeetValue!)
        self.heightInchesField.text = String(heightInchesValue!)
        self.weightField.text = String(weightValue!)
        self.birthdayField.text = birthdayValue!
        self.stepGoalField.text = String(stepGoalValue!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.settingsTitle.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
            options: [],
            animations: {
                self.settingsTitle.alpha = 1.0
            })
    }

    
    // Done button pressed for birthday TextField picker
    @objc func doneButtonPressed() {
        
       if let datePicker = self.birthdayField.inputView as? UIDatePicker {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .medium
           self.birthdayField.text = dateFormatter.string(from: datePicker.date)
       }
       self.birthdayField.resignFirstResponder()
    }

    @IBAction func logoutButtonPressed(_ sender: Any) {
        
        
        let firebaseAuth = Auth.auth()
        do {
            let defaults = UserDefaults.standard
            for i in 0..<defaultItemCount {
                defaults.removeObject(forKey: String(String(i)))
                defaults.removeObject(forKey: String(String(i)) + "x")
                defaults.removeObject(forKey: String(String(i)) + "y")
                defaults.removeObject(forKey: String(String(i)) + "Rotation")
            }
            defaults.removeObject(forKey: "itemCount")
            defaults.removeObject(forKey: "background")
            try firebaseAuth.signOut()
            self.performSegue(withIdentifier: "unwindToLogin", sender: self)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        
    }

    @IBAction func saveChangesButtonPressed(_ sender: Any) {
        
        // Re-saving changes to app-wide variables if user changed them
        heightFeetValue = Int(heightFeetField.text!)
        heightInchesValue = Int(heightInchesField.text!)
        weightValue = Int(weightField.text!)
        birthdayValue = birthdayField.text!
        nameValue = nameField.text!
        stepGoalValue = Int(stepGoalField.text!)
        petNameValue = petNameField.text!
        notificationsValue = notificationsField.isOn
        
        // get current logged in user
        // update changes in Firestore
        
        let user = Auth.auth().currentUser
        
        if let user = user {
            let email = user.email

            db.collection("users").whereField("email_address", isEqualTo: email!)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            
                            db.collection("users").document(document.documentID).updateData([
                                "birthday": self.birthdayField.text!,
                                "pet_name": self.petNameField.text!,
                                "height_feet": self.heightFeetField.text!,
                                "height_inches": self.heightInchesField.text!,
                                "weight" : self.weightField.text!,
                                "step_goal": self.stepGoalField.text!,
                                "notifs": self.notificationsField.isOn
                            ])
                        }
                    }
            }
        }
        
        let saveChangesAlert = UIAlertController(title: "", message: "Changed Saved!", preferredStyle: UIAlertController.Style.alert)
        saveChangesAlert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil))
        present(saveChangesAlert, animated: true, completion: nil)
    }
        

    @IBAction func deleteAccountButtonPressed(_ sender: Any) {
        let deleteAlert = UIAlertController(title: "Delete Account", message: "Are you sure you want to delete your account? This will permanently erase all data.", preferredStyle: UIAlertController.Style.alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action: UIAlertAction!) in
            
            let defaults = UserDefaults.standard
            for i in 0..<defaultItemCount {
                defaults.removeObject(forKey: String(String(i)))
                defaults.removeObject(forKey: String(String(i)) + "x")
                defaults.removeObject(forKey: String(String(i)) + "y")
                defaults.removeObject(forKey: String(String(i)) + "Rotation")
            }
            defaults.removeObject(forKey: "itemCount")
            defaults.removeObject(forKey: "background")
            
            let user = Auth.auth().currentUser
           
            // delete account info in database
            if let user = user {
               let email = user.email

                db.collection("users").whereField("email_address", isEqualTo: email!)
                   .getDocuments() { (querySnapshot, err) in
                       if let err = err {
                           print("Error getting documents: \(err)")
                       } else {
                           for document in querySnapshot!.documents {
                                db.collection("users").document(document.documentID).delete()
                        }
                    }
                }
            }
            
            // delete user in authentication
            user?.delete { error in
              if let error = error {
                print(error)
                // An error happened.
              } else {
                print("deleted")
              }
            }
            
           self.performSegue(withIdentifier: "unwindToLogin", sender: self)
        }))

        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        
        present(deleteAlert, animated: true, completion: nil)
    }
    
    // Code to dismiss keyboard when user clicks on background
    func textFieldShouldClear(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
                
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
