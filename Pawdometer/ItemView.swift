//
//  ItemView.swift
//  Pawdometer
//
//  Created by Tracy on 4/12/20.
//  Copyright © 2020 kimberly. All rights reserved.
//

import UIKit

class ItemView: UIImageView {
    
    var dragStartPositionRelativeToCenter : CGPoint?
    var vc : UIViewController
    var itemIndex : Int
    
    init(image: UIImage!, rootVC: UIViewController, index: Int) {
        vc = rootVC
        itemIndex = index
        super.init(image: image)
        
        self.isUserInteractionEnabled = true
        
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePan)))
        addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress)))
        addGestureRecognizer(UIRotationGestureRecognizer(target: self, action:     #selector(handleRotation)))
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 2
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func handlePan(nizer: UIPanGestureRecognizer!) {
        if nizer.state == UIGestureRecognizer.State.began {
            let locationInView = nizer.location(in: superview)
            dragStartPositionRelativeToCenter = CGPoint(x: locationInView.x - center.x, y: locationInView.y - center.y)
            
            layer.shadowOffset = CGSize(width: 0, height: 20)
            layer.shadowOpacity = 0.3
            layer.shadowRadius = 6
            
            return
        }
        
        if nizer.state == UIGestureRecognizer.State.ended {
            dragStartPositionRelativeToCenter = nil
            
            layer.shadowOffset = CGSize(width: 0, height: 3)
            layer.shadowOpacity = 0.5
            layer.shadowRadius = 2
            
            return
        }
        
        let locationInView = nizer.location(in: superview)
        
        UIView.animate(withDuration: 0.1) {
            self.center = CGPoint(x: locationInView.x - self.dragStartPositionRelativeToCenter!.x,
                                  y: locationInView.y - self.dragStartPositionRelativeToCenter!.y)
        }
        
        let defaults = UserDefaults.standard
        defaults.set(self.frame.origin.x, forKey: String(itemIndex) + "x")
        defaults.set(self.frame.origin.y, forKey: String(itemIndex) + "y")
    }
    
    @objc func handleLongPress(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            return
        }
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = 50
        shake.autoreverses = true
        shake.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        self.layer.add(shake, forKey: "position")
        
        let title = "Remove Item?"
        let message = "Are you sure you want to remove this item?"
        let ac = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac.addAction(cancelAction)

        let deleteAction = UIAlertAction(title: "Remove", style: .destructive, handler: { (action) -> Void in
            self.image = nil
            self.isUserInteractionEnabled = false
            
            // Remove items from user defaults
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: String(self.itemIndex))
            defaults.removeObject(forKey: String(self.itemIndex) + "x")
            defaults.removeObject(forKey: String(self.itemIndex) + "y")
            defaults.removeObject(forKey: String(self.itemIndex) + "Rotation")
        })
        ac.addAction(deleteAction)
        
        vc.present(ac, animated: true, completion: nil)
    }
    
    @objc func handleRotation(_ sender: UIRotationGestureRecognizer) {
        var lastRotation: CGFloat = 0
        var originalRotation = CGFloat()
        
        if sender.state == .began {
            sender.rotation = lastRotation
            originalRotation = sender.rotation
        } else if sender.state == .changed {
            let newRotation = sender.rotation + originalRotation
            sender.view?.transform = CGAffineTransform(rotationAngle: newRotation)
            
            // Save rotation to user defaults
            let defaults = UserDefaults.standard
            defaults.set(Double(newRotation), forKey: String(itemIndex) + "Rotation")
            defaults.set(self.frame.origin.x, forKey: String(itemIndex) + "x")
            defaults.set(self.frame.origin.y, forKey: String(itemIndex) + "y")
            
        } else if sender.state == .ended {
            lastRotation = sender.rotation // Save the last rotation
        }
    }
}
